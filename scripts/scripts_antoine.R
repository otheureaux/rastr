library(sf)
library(mapview)
library(tidyverse)

setwd("C:/Users/otheureaux/Desktop/antoine") # dossier source
list.files()

# Spatialisation de donnees issues d'un fichier CSV ----

donnees <- read.csv2("BLE.csv", sep=",", header = F)
donnees <- st_as_sf(x = donnees, 
                    coords =  c("V6", 
                              "V5"), 
                    crs=4326)
plot(st_geometry(donnees))

# Géotraitement ----
emprise <- st_read("zone_lachapelle_WGS84.gpkg")
plot(emprise)

# st_intersects
donnees_couper <- st_intersection(donnees, emprise) # découper
plot(st_geometry(donnees_couper))

# enregistrer le résultats au format GPKG
st_write("BLE_couper.gpkg")

# lire un CSV
donnees <- read.csv2("mesures_antoine.csv", sep=";", header = T)

# fusionner deux colonnes
donnees <- mutate(donnees, date_heure = paste(donnees$date, donnees$heure))
write.csv2(donnees, "mesures_antoine_fusion.csv")

# Traitement décembre ----
setwd("C:/Users/otheureaux/Desktop/antoine") # dossier source
library(tidyverse)
library(sf)
library(tmap)
donnees <- read.csv2("decembre/tableau.csv", sep=";", header = F)
donnees <- donnees[-c(1:9),-c(1:2)] # on supprime les 9 premières lignes et les 2 premières colonnes
colnames(donnees) <- c("col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8") # ici noms des colonnes
donnees$col1 <- substring(donnees$col1, 1,19) # modification de la colonne date et heure
donnees <- donnees %>%
  group_by(col1) %>%
  summarise_all(funs(str_c(unique(.),collapse="")))
donnees$date <- substring(donnees$col1, 1,10) # ajout bon format date
donnees$heure <- substring(donnees$col1, 12,19) # ajout bon format heure
donnees <- donnees[,-1] # on supprime la première colonne inutile
donnees <- donnees[,c("date", "heure", "col2", "col3", "col4", "col5", "col6", "col7", "col8")] # on réorganise les colonnes
donnees$col2 <- substring(donnees$col2, 1,9) # modification de la colonne date et heure
donnees$col3 <- substring(donnees$col3, 1,7) # modification de la colonne date et heure
donnees$col4 <- substring(donnees$col4, 1,4) # modification de la colonne date et heure
donnees$col5 <- substring(donnees$col5, 1,4) # modification de la colonne date et heure
donnees$col6 <- substring(donnees$col6, 1,4) # modification de la colonne date et heure
donnees$col7 <- substring(donnees$col7, 1,4) # modification de la colonne date et heure
donnees$col8 <- substring(donnees$col8, 1,4) # modification de la colonne date et heure
donnees$date <- as.Date(donnees$date)
donnees$col2 <- as.numeric(donnees$col2)
donnees$col3 <- as.numeric(donnees$col3)
donnees$col4 <- as.numeric(donnees$col4)
donnees$col5 <- as.numeric(donnees$col5)
donnees$col6 <- as.numeric(donnees$col6)
donnees$col7 <- as.numeric(donnees$col7)
donnees$col8 <- as.numeric(donnees$col8)
colnames(donnees) <- c("date", "heure", "Y", "X", "TF", "PM1", "PM10", "PM2.5", "HR")
donnees$annee <- substring(donnees$date, 1,4)
donnees$mois <- substring(donnees$date, 6,7)
donnees$jour <- donnees$date <- substring(donnees$date, 9,10)
donnees$date2 <- paste0(donnees$jour, "/", donnees$mois, "/", donnees$annee)
donnees <- mutate(donnees, date_heure = paste(donnees$date2, donnees$heure))
donnees <- donnees[,-c(1:2)]
donnees <- donnees[,-c(8:11)]
donnees <- donnees[,c("date_heure", "Y", "X", "TF", "HR", "PM1", "PM2.5", "PM10")]
donnees # on affiche le tableau final
write.csv2(donnees, "nouveau_tableau8.csv")

# Ficher spatial ----
donnees <- read.csv2("nouveau_tableau8.csv", sep=";", header = T)
donnees_spatial <- sf::st_as_sf(x = donnees,                         
                           coords = c("X", "Y"),
                           crs = 4326)
plot(st_geometry(donnees_spatial))

tmap_mode("view")
tm_basemap(c(leaflet::providers$Esri.WorldTopoMap,
             leaflet::providers$OpenStreetMap, 
             leaflet::providers$GeoportailFrance.orthos)) + 
  tm_shape(donnees_spatial) + tm_dots(size = 0.2)


# NEW ----

